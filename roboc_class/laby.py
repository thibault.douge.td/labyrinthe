#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
from data.settings import SETTINGS


class Laby:
    nb_wall = 0
    nb_door = 0
    coord_x_end = 0
    coord_y_end = 0
    taille_x = 0
    taille_y = 1

    @staticmethod
    def victory():
        print("victory")
        sys.exit()

    def recup_map():
        map = open("../cartes/facile.txt", "r")
        map = map.readlines()
        return map

    def move_is_possible(self, y, x):
        map = Laby.recup_map()
        print(map[y])
        print(map[y][x])

        if map[y][x] == SETTINGS["map_symbols"]["wall"]:
            return False
        if map[y][x] == SETTINGS["map_symbols"]["exit"]:
            return Laby.victory()
        return True

    def dimensions(self):
        map = Laby.recup_map()
        self.taille_y = len(map) - 1
        self.taille_x = len(map[0]) - 1


if __name__ == "__main__":
    toto = Laby()
    toto.move_is_possible(5, 9)
