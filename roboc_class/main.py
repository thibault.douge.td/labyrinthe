#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
from data.settings import SETTINGS

if __name__ == "__main__":
    input_format_check = re.compile("^(?P<Direction>[a-zA-Z])(?P<Distance>\d*)$")
    while True:
        try:
            ans = input("Toto ? ")
            res = input_format_check.match(ans)
            direction = res.group("Direction").upper()
            try:
                distance = int(res.group("Distance"))
            except ValueError:
                distance = 1
            print("Direction = {}".format(direction))
            print("Distance  = {}".format(distance))
        except Exception as e:
            print("{} - {}".format(type(e), e))
        else:
            print("On sort de la boucle")
            try:
                robot_move = SETTINGS["valid_movement_options"][direction]
            except KeyError:
                robot_move = None
            if robot_move is not None:
                if move_is_possible(direction, distance):
                    robot_move(distance)

