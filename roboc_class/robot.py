#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from data.settings import SETTINGS


class Robot:
    """
    """
    # Des attributs
    ROBOT_SET = "valid_movement_options": {
            "up": {
                "letter": "N",
                "function": Robot.move_up,
            },
            "down": {
                "letter": "S",
                "function": Robot.move_down,
            },
            "right": {
            "letter": "E",
            "function": Robot.move_right,
        },
            "left":{
            "letter": "O",
            "function": Robot.move_left,
       },
    }

    activated = True
    coord_x = 0
    coord_y = 0
    nom = ""

    def __init__(self, nom):
        self.nom = nom

    def move_up(self, distance):
        """Deplacements du robot"""
        self.coord_y += distance

    def move_down(self, distance):
        """Deplacements du robot"""
        self.coord_y -= distance

    def move_left(self, distance):
        """Deplacements du robot"""
        self.coord_x -= distance

    def move_right(self, distance):
        """Deplacements du robot"""
        self.coord_x += distance

    @staticmethod
    def get_movement_function(letter):
        for v in SETTINGS["valid_movement_options"].values():
            if v["letter"] == letter:
                return v["function"]
        return None
